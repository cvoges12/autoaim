#include <iostream>     // std::cout
#include <windows.h>    // Sleep, GetPixel
#include <winuser.h>    // SetCursorPos, mouse_event, GetAsyncKeyState, GetDC, HDC
#include <thread>       // Threads

typedef struct {
    int x;
    int y;
} Point;

void welcomeMessage() {
    std::cout << "\n \n \n";
    std::cout << "         ____ ____ ____ ____ ____ ____ ____ \n";
    std::cout << "        ||B |||o |||p |||z |||. |||i |||o || \n";
    std::cout << "        ||__|||__|||__|||__|||__|||__|||__|| \n";
    std::cout << "        |/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\| \n";
    std::cout << "        ||A |||u |||t |||o |||A |||i |||m || \n";
    std::cout << "        ||__|||__|||__|||__|||__|||__|||__|| \n";
    std::cout << "        |/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\| \n";
    std::cout << "\n \n \n";
    std::cout << "        Made by Midi & Kilian we Hope you enjoy = }";
}


void click(Point* enemy) {
    // dial in the timings
    int x;
    int y;
    while (true) {
        x = enemy->x;
        y = enemy->y;
        if (!(x <= 0 || y <= 0)) {
            SetCursorPos(x, y);
            mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            Sleep(2);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
            Sleep(1);
            mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
            Sleep(1);
            //std::cout << "DEBUG: found person";
        }
    }
}

void radialSearch(const int width, const int height, DWORD* pPixels, Point* enemy) {

    const int centerX = (int)width / 2;
    const int centerY = (int)height / 2;

    const int stepSize = 4;

    int x = centerX;
    int y = centerY;

    int dx = 1;
    int dy = 0;

    int steps = 1;
    while (x < width - stepSize) {
        if (dx == 1) {
            if (x > (steps * stepSize + centerX)) {
                dx = 0;
                dy = -1;
            }
        }
        else if (dx == -1) {
            if (x < (steps * stepSize - centerX)) {
                dx = 0;
                dy = 1;
            }
        }
        else if (dy == 1) {
            if (y > (steps * stepSize + centerY) || y >= height - stepSize) {
                dy = 0;
                dx = 1;
                steps++;
            }
        }
        else {
            if (y < (steps * stepSize - centerY) || y <= stepSize) {
                dy = 0;
                dx = -1;
            }
        }

        x += dx;
        y += dy;

        switch ((pPixels[x + y * width])) {
            // skin colors
        case(0xffffead3):   //0x00d3eaff
        case(0xfffadab8):   //0x00b8dafa
        case(0xffdeb991):   //0x0091b9de
        case(0xffbd8e68):   //0x00688ebd
            //case(0xff9a643c):   //0x003c649a
        case(0xff584539):   //0x00394558
            *enemy = { x, y };
            return;

        default:
            continue;
        }


    }
    *enemy = { 0, 0 };
}


void searchThread(const int width, const int height, DWORD* pPixels, Point* enemy) {

    while (true) {
        radialSearch(width, height, pPixels, enemy);
    }
}


    const HDC displayHandle = GetDC(NULL);
    const HDC hMemoryDC = CreateCompatibleDC(displayHandle);

    BITMAPINFO bmi = {
        bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader),
        bmi.bmiHeader.biWidth = GetDeviceCaps(displayHandle, HORZRES),
        bmi.bmiHeader.biHeight = -GetDeviceCaps(displayHandle, VERTRES),
        bmi.bmiHeader.biPlanes = 1,
        bmi.bmiHeader.biBitCount = 32
    };

    Point enemy = { 0, 0 };

    const int displayWidth = GetSystemMetrics(SM_CXSCREEN);
    const int displayHeight = GetSystemMetrics(SM_CYSCREEN);

    DWORD* pPixels = static_cast<DWORD*>(GlobalAlloc(GMEM_FIXED, displayWidth * displayHeight * 4));

    HBITMAP hBitmap = CreateCompatibleBitmap(displayHandle, displayWidth, displayHeight);


int main() {

    SelectObject(hMemoryDC, hBitmap);

    welcomeMessage();

    std::thread  threadClicker(click, &enemy);

    std::thread  threadAimer(searchThread, displayWidth, displayHeight, pPixels, &enemy);

    while (true) {
        BitBlt(hMemoryDC, 0, 0, bmi.bmiHeader.biWidth, -bmi.bmiHeader.biHeight, displayHandle, 0, 0, SRCCOPY);

        GetDIBits(displayHandle, hBitmap, 0, displayHeight, pPixels, &bmi, DIB_RGB_COLORS);
    }

}





